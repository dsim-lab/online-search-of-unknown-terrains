function [coverage_time] = ...
    ArnoldLogistic_coverage(A,B,C,...
                            r,v,...
                            IC_vec,...
                            x0,...
                            c,f,sr,...
                            xmin,xmax,ymin,ymax,...
                            obs,...
                            nc,nr,dsi,tmax)
% ARNOLDLOGISTIC_COVERAGE Uses a hybrid chaotic system to search 2D
%     rectangle.
% 
% Usage: coverage_time = ArnoldLogistic_coverage(A,B,C,r,v,IC_vec,x0,c,f,sr,xmin,xmax,ymin,ymax,obs,nc,nr)
%
% Description: Computes the robot's trajectory and coverage time for a robot
%              to cover 90% of the map with scaled Arnold-Logistic dynamical
%              system is used to compute the robot's trajectory.
% 
% Input:
%     A,B,C      = Arnold system parameters.
%     r          = chaotic parameters for the Logistic map system.
%     v          = robot's velocity.
%     IC_vec     = vector of Arnold and robot system initial conditions.
%     x0         = dynamic IC for the Logistic map.
%     c          = coverage rate criterion.
%     f          = scaling factor.
%     sr         = scaning range of sensors
%     xmin, xmax = X dimension limits for 2D environment
%     ymin, ymax = Y dimension limits for 2D environment
%     obs        = n rectangular obstacles stored in 2x2n matrix
%     nc, nr     = number of columns and rows of the grid zones
% 
% Output:
%     coverage_time = time taken to cover 90% of the map. The output comes 
%                     along with the map showing the number of each cells
%                     covered and the computational time
%     
% Author: Karan Sridharan
%         Patrick McNamee
% Date: Unknown
% 
% Edit:
%     09/13/2021 -- Reformated code and updated documentations (PM)
%

%% Constant set ups
% Coverages
tot_covrate = 0;  %Total coverage rate.
tot_covrate_vec = ones(1,2)*tot_covrate;  % Last 2 total coverage rates
total_coverage_vector = 0;  % Stores total coverage rate for the video

%Time step
dt = 0.1; dt_half = dt/2;

% Map cell division set up
nx_cells = ceil((xmax - xmin)/sr);           % number of x divisions
ny_cells = ceil((ymax - ymin)/sr);           % number of y divisions
xpoints  = linspace(xmin,xmax,nx_cells + 1);  % grid x cell points
ypoints  = linspace(ymin,ymax,ny_cells + 1);  % grid y cell points

% Determine what cells are reachable and what cells are within obstacles.
reachable_cells = ones(nx_cells, ny_cells);
if ~isempty(obs)  % If there are any obstacles in the environment
    for i=1:2:size(obs,2)
        % Determine if cell's x points are in obstacle's projection.
        row_blocked = and(xpoints(1:end-1) >= obs(1, i),...
                          xpoints(2:end) <= obs(2, i));
        % Determine if cell's y points are in obstacle's projection.
        column_blocked = and(ypoints(1:end-1) >= obs(1, i + 1),...
                             ypoints(2:end) <= obs(2, i + 1));
        % A cell is within obstacle (i.e. inaccessible) all x and y points
        % are within the obstacles.
        cell_inaccessable = and((ones(ny_cells,1)*row_blocked)',...
                                 ones(nx_cells,1)*column_blocked);
        % Keep track of all reachable cells
        reachable_cells = and(reachable_cells, not(cell_inaccessable));
    end
end

total_cells = sum(reachable_cells, 1:2); % Total number of reachable cells.
unvisited_cells = total_cells;           % Cells left to visit.
cell_visit_map = zeros(nx_cells,ny_cells); % Boolean to keep of visits.
prev_cells_covered = 0;                  % Total count.

% Note obstacle avoidance buffer distance
obs_buffer = 1/2;

% Number of iterations to generate new dynamic and robot
%entries using the Arnold system
nn = 1001; 

%Criterion to check the accuracy of points as simulation time increases. This
%reduces numerical error
epsilon = 1e-4;

%Adding unscaled five square obstacles to the map
obs_vec_unscaled = obs/f;

count = 0;  % how many times the robot hits an obstacle
count_max = 100;  % maximum allowable times the robot can hit an obstacle
count_maxiter = 5;  % increases count_max in case the robot is unable to visit all zones

% Adjust 

% Index number for storing the x and y coordinates of the Robot's trajectory
m_storage = 1;
n_storage = 0;

%time_count counts the coverage time. Desired coverage rate is how much the
%robot needs to cover the entire cell_visit_map. Desired coverage rate of
%53.1% is equivalent to 90% as there are obstacles which reduce the total
%area
time_count = 0;
des_covrate = 0.90;

tic;

%index to change the angle of the robot's trajectory if the coverage rate at
%the current iteration does not change from the last iteration
% index = 3;
index = dsi;

while tot_covrate <  des_covrate
    %To mirrormap robot coordinates that are out of bounds, the dynamical equation 
    %must be discretized. Therefore, 4th order Runge Kutta method was used to
    %discretize the dynamical system 

    %At every iteration, the new coverage rate gets assigned to the previous
    %coverage rate in order to decide whether the robot needs to change its
    %angle

    tot_covrate_vec(1) = tot_covrate_vec(2);
    
    
    %Adaptive RK4 is used to increase accuracy in points
    %as simulation time increses
    for i = 1:nn-1
        %scales the first entries of the robot IC and adds to a temporary
        %matrix called IC_vec_tmp
        IC_vec_tmp(i,1) = f*IC_vec(i,4);
        IC_vec_tmp(i,2) = f*IC_vec(i,5);
        %generates new dynamic and robot entries based on current and half the time step
        [x,y,z,X,Y] = arnold_RK4(...
            A,B,C,...
            v,...
            IC_vec(i,1),IC_vec(i,2),IC_vec(i,3),...
            IC_vec(i,4),IC_vec(i,5),...
            dt,index);
        [x_half,y_half,z_half,X_half,Y_half] = arnold_RK4(...
            A,B,C,...
            v,...
            IC_vec(i,1),IC_vec(i,2),IC_vec(i,3),...
            IC_vec(i,4),IC_vec(i,5),...
            dt_half,index);
        %compares whether the robot entries associated with half or current
        %step size is more accurate
        if abs(X-X_half)/dt > epsilon || abs(Y-Y_half)/dt > epsilon
            %in this case, the robot entries assoiated with current step
            %size is not accurate and assigns new entries associated with
            %half step size to the vector IC_vec
            dt = dt_half;  % TODO: figure out why dt_half isn't updated
            IC_vec(i+1,1) = x_half;
            IC_vec(i+1,2) = y_half;
            IC_vec(i+1,3) = z_half;
            IC_vec(i+1,4) = X_half;
            IC_vec(i+1,5) = Y_half;
        else
            %in this case, the robot entries assoiated with current step
            %size is accurate and assigns new entries associated with
            %current step size to the vector IC_vec
            IC_vec(i+1,1) = x;
            IC_vec(i+1,2) = y;
            IC_vec(i+1,3) = z;
            IC_vec(i+1,4) = X;
            IC_vec(i+1,5) = Y;
        end
        
        %if the points are out of bounds, mirrormap function will map those
        %points in bounds. The bounds are divided by scaling factor as its
        %values are scaled in the beginning of the simulation
        [IC_vec(end,4),IC_vec(end,5)] = ...
            mirrormap(...
                IC_vec(end,4),IC_vec(end,5),...
                xmin/f,xmax/f,ymin/f,ymax/f ...
            );

        if ~isempty(obs)
            % the below function prevents the robot's path from coming
            % close to any obstacles.
            [IC_vec(end,4),IC_vec(end,5)] = ...
                mirrormap_obs_arnold(...
                    IC_vec(end,4)*f,IC_vec(end,5)*f,obs,...
                    xmin, xmax, ymin, ymax,...
                    obs_buffer...
                );
            IC_vec(end,4:5) = IC_vec(end,4:5)/f;
        end
        %scales the new robot trajectory entries and assigns to IC_vec_tmp 
        IC_vec_tmp(i+1,1) = f*IC_vec(i+1,4);
        IC_vec_tmp(i+1,2) = f*IC_vec(i+1,5);
        
        %All of the dynamic and robot coordinates must not have NaN or infinity values
        % Simplified with built in MATLAB functions -- PM
        if ~all(isfinite(IC_vec(i+1,:)))
            error('Cannot have NaN or Inf values')
        end
        
        %time count iterates at step size of dt
        time_count = time_count + f*dt;
            
    end
        
    %If the robot's coordinates are still out of bounds after using the Mirror
    %Map technique, the code will stop executing. Robot's coordinates must
    %lie within bounds
    % Removed for loop for MATLAB input functions -- PM
    if any(IC_vec_tmp(:,1) < xmin) || any(IC_vec_tmp(:,2) < ymin) || ...
       any(IC_vec_tmp(:,1) > xmax) || any(IC_vec_tmp(:,2) > ymax)
        error('Coordinates are out of bounds')
    end
    
    % Check to make sure that the robot is not within any obstacles. If it
    % is then there is an error the mirror mapping techniques.
    for i=1:2:size(obs,2)
        if any((IC_vec_tmp(:,1) > obs(1,i))   & ...
               (IC_vec_tmp(:,1) < obs(2,i))   & ...
               (IC_vec_tmp(:,2) > obs(1,i+1)) & ...
               (IC_vec_tmp(:,2) < obs(2,i+1)))
            error('Coordinate within an obstacle.')
        end
    end

    %At every iteration, new values in IC_vec will replace old values.
    %Therefore we are storing old values in a storage vector pX_storage and
    %pY_storage
    n_storage = n_storage + length(IC_vec_tmp);
    pX_storage(m_storage:n_storage) = IC_vec_tmp(:,1);
    pY_storage(m_storage:n_storage) = IC_vec_tmp(:,2);
    
    %Index will change according to the length of the vector containing the
    %new values 
    m_storage = n_storage+1; 
    
    %The function cellmap will now calculate how many cells the robot has now
    %visited using the Arnold system. Extra output arguments are new cells covered and
    %rep cells covered
    [cell_visit_map, new_cells_covered_arnold, rep_cells_covered_arnold] = ...
        cellmap(...
            xpoints,ypoints,...
            IC_vec_tmp(:,1),IC_vec_tmp(:,2),...
            cell_visit_map, reachable_cells...
        );
    tot_cells_covered = new_cells_covered_arnold + rep_cells_covered_arnold; %calculates total cells covered
    prev_cells_covered = prev_cells_covered + new_cells_covered_arnold; %calculates previous cells covered
    tot_covrate = prev_cells_covered/total_cells; %Calculates the total coverage rate
    tot_covrate_vec(2) = tot_covrate;
    
     % Adds the entries of the total coverage rate to the vector
    if length(total_coverage_vector) == 1
        total_coverage_vector(2:length(IC_vec)) = tot_covrate;
        index_a = length(IC_vec);
    else
        index_b = index_a + length(IC_vec);
        total_coverage_vector(index_a:index_b) = tot_covrate;
        index_a = index_b;
    end
    
    %if the total coverage rate exceeds the desired coverage rate, the
    %simulation will stop
    if tot_covrate >=  des_covrate
        break;
    end
   
    %A criteiron is used to determine if the robot needs to change zones for
    %its coverage task
    if new_cells_covered_arnold/tot_cells_covered >= c*(unvisited_cells/total_cells)
        %We get the last trajectory points as our new IC for the next iteration
        
        %If the last 2 iteration coverage rates are same then the angle for the
        %robot's trajectory will change. If not, the angles will not change
        if tot_covrate_vec(2) - tot_covrate_vec(1) == 0 
            index = mod(index, 3) + 1;
        end
        %We get the last trajectory points as our new IC for the next iteration
        %IC_vec = [IC_vec(end,1),IC_vec(end,2),IC_vec(end,3),IC_vec(end,4),IC_vec(end,5)];
        IC_vec = IC_vec(end,:);
        %We get the last scaled trajectory points as our new scaled robot
        %IC for the next iteration
        IC_vec_tmp = IC_vec_tmp(end,:);% [IC_vec_tmp(end,1),IC_vec_tmp(end,2)];
        %unvisited cells reduce by new cells covered
        unvisited_cells = unvisited_cells - new_cells_covered_arnold;
        
    else    
        %the following code constructs a collision free path and ensures
        %the robot lands to a zone, free of obstacles
        while tot_covrate < des_covrate
            %In this case, the robot wants to move to a different zone. The
            %following lines of code constructs a path for the robot to
            %navigate using the Logistic map
        
            %At every iteration, we reset the coordinates as an empty list
            x = []; X = []; Y = []; X_rel = []; Y_rel = []; X_map = [];Y_map = [];
            x(1) = x0; 
            %Assigning the robot IC of the Logistic map system
            X(1) = IC_vec_tmp(end,1); Y(1) = IC_vec_tmp(end,2);

            X_begin = X(1); Y_begin = Y(1);
            
            %generates a list of the zones with their respective density
            %and distance from the current robot's position
            [zone, probzone_vec] = ...
                prob_zone(...
                    cell_visit_map, reachable_cells, ...
                    xmin,xmax,ymin,ymax, ...
                    X_begin,Y_begin, ...
                    nr,nc,sr ...
                );
            
            %removes the entry, whose zone corresponds to the zone the
            %robot is current positioned
            for i = 1:size(probzone_vec,1)-1
                if zone == probzone_vec(i,1)
                    probzone_vec(i,:) = []; 
                end
            end
            
            %loops to generate the robot's path, ensuring it lands at a
            %zone free of obstacles
            while ~isempty(probzone_vec)
                %finds the zone associated to the minimum distance
                k = find(probzone_vec(:,5) == min(probzone_vec(:,5)));
                %if there are multiple zones associated with the minimum
                %distance, the following code selects the zone associated
                %with the minimum density as well as minimum distance
                if length(k) ~= 1
                    k = find(probzone_vec(k,2) == min(probzone_vec(k,2)));
                    if length(k) > 1
                        k = k(1);
                    end
                end
                %assigns the midpoint associated with the desired zone to
                %X_end and Y_end
                X_end = probzone_vec(k,3); X_midpoint_desired = probzone_vec(k,3);
                Y_end = probzone_vec(k,4); Y_midpoint_desired = probzone_vec(k,4);
                d = probzone_vec(k,5);
                
%                 % Ensure X_end and Y_end are valid positions -- PM
%                 if ~isempty(obs)
%                     for i_obs = 1:2:length(obs)
%                         [X_end, Y_end] = ...
%                             mirrormap_obs_arnold(...
%                                 X_end,Y_end,obs(1:2,i_obs:i_obs+1),...
%                                 xmin,xmax,ymin,ymax,obs_buffer...
%                                 );
%                     end
%                 end

                %the following constructs a time vector to determine the time taken
                %for the robot to navigate between the current zone and the new zone
                dt_logistic = 0.1; %step size for Logistic map
                time_taken = (0:dt_logistic:d/v); %time vector
                %Generating new dynamic and robot entries using the Logistic map
                [x,X,Y] = LogisticEquation(r,v,x,X,Y,xmin,xmax,ymin,ymax,time_taken,dt_logistic);
                
                %Using the Logistic map points, the robot travels to a random zone. Since
                %we want the robot to navigate between two specific zones, we generate a mapping
                %equation to map the Logistic map points as a chaotic line between two points

                %Generating relative trajectory points to map the Logistic map points
                %as a line between two given points.
                X_rel = linspace((X_begin-X(1)),(X_end-X(end)),length(X));
                Y_rel = linspace((Y_begin-Y(1)),(Y_end-Y(end)),length(Y));
                
                %Generating the mapped trajectory points
                for i = 1:length(X)
                    X_map(i) = X(i) + X_rel(i);
                    Y_map(i) = Y(i) + Y_rel(i);
                    %translates the mapped trajectory points that are out of bounds
                    %inside the map
                    [X_map(i),Y_map(i)] = ...
                        mirrormap(X_map(i),Y_map(i),xmin,xmax,ymin,ymax);
                    %If the Robot's relative coordinates are out of bounds, 
                    %the code will stop executing. Robot's coordinates must
                    %lie within bounds
                    if X_map(i) < xmin || Y_map(i) < ymin || X_map(i) > xmax || Y_map(i) > ymax
                        error('Coordinates are out of bounds')
                    end
                end

                %All of the dynamic and robot coordinates must not have NaN or infinity values
                if isnan(X(:,1)) && isnan(Y(:,1))
                    error('Cannot have NaN values')
                end
                
               %checks whether the robot's path crosses in any of the
               %obstacles
               if ~isempty(obs)

                   for i = 1:length(X_rel)
                       % Check each obstacles individually
                       for i_obs = 1:2:length(obs)
                           [Xmirror, Ymirror] = ...
                               mirrormap_obs_arnold(...
                               X_map(i),Y_map(i),obs(1:2,i_obs:i_obs+1),...
                               xmin,xmax,ymin,ymax,obs_buffer...
                               );
                           % Object boundaries
                           osl = obs(1,i_obs);
                           osr = obs(2,i_obs);
                           osb = obs(1,i_obs+1);
                           ost = obs(2,i_obs+1); 
                           if X_map(i) ~= Xmirror || Y_map(i) ~= Ymirror
                               break
                           end
                       end
                       if X_map(i) > osl - obs_buffer && X_map(i) < osr + obs_buffer &&...
                          Y_map(i) > osb - obs_buffer && Y_map(i) < ost + obs_buffer
                               %removes entries that start from the point that
                               %intefers with an obstacle till the midpoint
                               %coordinate
                               time_count = time_count + dt_logistic;
                               X_map([i+1:end]) = [];
                               Y_map([i+1:end]) = [];
                               %translates the point away from the obstacle using the below function
                               X_rel = []; Y_rel = [];
                               [X_map(i),Y_map(i)] = ...
                                   mirrormap_obs_arnold(X_map(i),Y_map(i),obs,xmin,xmax,ymin,ymax,obs_buffer);
                               %uses the below function to construct a
                               %collision-free path
                               [X_map, Y_map, count, time_count] = ...
                                   mirrormap_obs_logistic(...
                                       X_rel,Y_rel,X,Y,...
                                       X_map,Y_map,X_end,Y_end,...
                                       i,...
                                       osl,osr,osb,ost,...
                                       xmin, xmax, ymin, ymax,...
                                       obs, v,...
                                       time_count,dt_logistic,count_max, obs_buffer);
                               break;
                       
                       else
                           %in this case, the points are free from obstacles
                           %and increases the coverage time
                           time_count = time_count + dt_logistic;
                       end
                   end
               end

                % Check to make sure that the robot is not within any obstacles. If it
                % is then there is an error the mirror mapping techniques.
                for i=1:2:size(obs,2)
                    if any((X_map(:,1) > obs(1,i))   & ...
                           (X_map(:,1) < obs(2,i))   & ...
                           (Y_map(:,2) > obs(1,i+1)) & ...
                           (Y_map(:,2) < obs(2,i+1)))
                        error('Coordinate within an obstacle.')
                    end
                end
                
                %At every iteration, new values in pX and pY will replace old values.
                %Therefore we are storing old values in a storage vector
                n_storage = n_storage + length(X_map);
                pX_storage(m_storage:n_storage) = X_map; pY_storage(m_storage:n_storage) = Y_map;

                %Index will change according to the length of the vector containing the
                %new values 
                m_storage = n_storage+1; 

                %The function cellmap will now calculate how many cells the robot has now
                %visited using Logistic map. Extra output arguments are new cells covered and
                %rep cells covered
                [cell_visit_map, new_cells_covered_logistic, rep_cells_covered_logistic] = ...
                    cellmap(xpoints,ypoints,X_map,Y_map,cell_visit_map,reachable_cells);
                
                %We get the last trajectory points as our new IC for the next iteration
                IC_vec = [IC_vec(end,1),IC_vec(end,2),IC_vec(end,3),X_map(end)/f,Y_map(end)/f];
                %We get the last scaled trajectory points as our new scaled robot
                %IC for the next iteration
                IC_vec_tmp = [X_map(end),Y_map(end)];
                prev_cells_covered = prev_cells_covered + new_cells_covered_logistic; %calculates previous cells covered
                tot_covrate = prev_cells_covered/total_cells; %Calculates the total coverage rate
                tot_covrate_vec(2) = tot_covrate;
                
                % Adds the entries of the total coverage rate to the vector
                index_b = index_a + length(X_map);
                total_coverage_vector(index_a:index_b) = tot_covrate;
                index_a = index_b;
                
               %breaks the while loop when the robot reaches the desired
               %zone free of obstacles
                 if X_map(end) == X_end && Y_map(end) == Y_end
                    break
                 end
                %if the robot has exceeded its rate of coverage
                if tot_covrate > des_covrate
                    break
                end
                %in this case, the robot has not reached the desired zone.
                %So, it generates a new best zone for the robot to reach
                if count >= count_max
                    x = []; X = []; Y = []; X_rel = []; Y_rel = []; X_map = [];Y_map = [];
                    x(1) = x0; X(1) = IC_vec_tmp(end,1); Y(1) = IC_vec_tmp(end,2);
                    X_begin = X(1); Y_begin = Y(1);
                    k = find(X_midpoint_desired == probzone_vec(:,3) &...
                             Y_midpoint_desired == probzone_vec(:,4));
                    probzone_vec(k,:) = [];
                     %in case the robot has not reached all zones, count_max
                    %will iterate so that the robot can spend more time in
                    %avoiding obstacles and reach to the desired zone
                    if isempty(probzone_vec)
                        count_max = count_max + count_maxiter;
                    end
                end     
            end
            %if all zones have been eliminated from the list, it will
            %regenerate a new list
            if isempty(probzone_vec) && count == count_max
                continue;
            elseif isempty(probzone_vec)
                break;
            end
            if (X_end == X_map(end)) && (Y_end == Y_map(end))
                break
            end
            if (count < count_max) || (tot_covrate > des_covrate)
                break
            end
        end
    end
    
    if time_count >= tmax
        error("Time limit reached")
    end
end
toc;

%Plotting the midpoint coordinates of each zone 
coverage_time = time_count;

%The following lines of code makes the video for the simulation. To stop
%generating video while running simulation, suppress lines 419-445
fig = figure;
set(gcf,...
    'color','w', 'Position', [100, 100, 500, 500]);
t = linspace(0,coverage_time,length(pX_storage)); 
my_writer = VideoWriter('arnoldlogisticscaled_5obs_200x200_SR4m.avi');
open(my_writer);
for k = 1:1000:length(t)
    clf;
    plot(pX_storage(k),pY_storage(k),'r*','LineWidth',3);
    hold on
    plot(pX_storage(1:k),pY_storage(1:k),'b-','LineWidth',1);
    % Plot obstacles (Can obstruct centers)
    for i=1:2:size(obs,2)
        obsi = obs(:,i:i+1)';
        obsi = obsi(:)';
        obsi(3:4) = obsi(3:4) - obsi(1:2);
        rectangle('Position',obsi,'FaceColor','k','EdgeColor','k','LineWidth',1)
    end
    % Plot horizontal lines            
    for i=1:(nr+1)
        line([xmin, xmax], ymin + (i - 1)/nr*(ymax - ymin)*ones(1,2),...
            'Color','k','LineWidth',2);
    end
    % Plot vertical lines
    for i=1:(nc+1)
        line(xmin + (i - 1)/nc*(xmax - xmin)*ones(1,2), [ymin, ymax],...
            'Color','k','LineWidth',2);
    end
    axis([xmin xmax ymin ymax])
    xlabel('X(m)'); ylabel('Y(m)')
    view([0 90]);
%     title(['Time: ',num2str(t(k)),'s',', Total coverage rate: ',num2str(total_coverage_vector(k)),...
%         ', Initial index: ',num2str(int_index),', c value: ',num2str(c)])
title(['Time: ',num2str(t(k)),'s',', Total coverage rate: ',num2str(total_coverage_vector(k))])
    frame = getframe(fig);
    writeVideo(my_writer,frame);
end
close(my_writer);

%1st plot- robot's trajectory
figure(1)
plot(pX_storage,pY_storage,'b-','LineWidth',5);
axis([xmin xmax ymin ymax])
xlabel('X(m)'); ylabel('Y(m)')

hold on
% Plot Zone Centers
for i=1:nc
    for j=1:nr
        plot(...
            xmin + (i - 0.5)/nc*(xmax - xmin),...
            ymin + (j - 0.5)/nr*(ymax - ymin),...
            'ro','LineWidth',8);
    end
end
% Plot obstacles (Can obstruct centers)
for i=1:2:size(obs,2)
    obsi = obs(:,i:i+1)';
    obsi = obsi(:)';
    obsi(3:4) = obsi(3:4) - obsi(1:2);
    rectangle('Position',obsi,'FaceColor','k','EdgeColor','k','LineWidth',1)
end
% Plot horizontal lines
for i=1:(nr+1)
    line([xmin, xmax], ymin + (i - 1)/nr*(ymax - ymin)*ones(1,2),...
        'Color','k','LineWidth',4);
end
% Plot vertical lines
for i=1:(nc+1)
    line(xmin + (i - 1)/nc*(xmax - xmin)*ones(1,2), [ymin, ymax],...
        'Color','k','LineWidth',4);
end

% 2nd plot- color map to denote the number of cells the Robot has visited
figure(2)
pcolor(linspace(xmin,xmax,nx_cells),linspace(ymin,ymax,ny_cells),cell_visit_map);
colorbar
colormap(jet(1000))
grid on
xlabel('X(m)');ylabel('Y(m)')
end
