%% File Information
% 
% Author: Karan Sridharan
%         Patrick McNamee
% File: Arnold_coverage_rand.m
% Date: Unknown
% 
% Description: Script to initialize an Arnold Logistic search. Sets the
%              various system parameters as well as initial conditions for
%              the search.
%
% Edits:
%     09/13/2021: Rewrite of comments and code clean up -- Patrick McNamee
%     02/23/2022: Added in randomly generated environment capability -- PM
%
%% Blank-slate MATLAB environment
clc; clear; close all;

%% Specify the robotic platform parameters
% Robot system
% X0 = 0; Y0 = 0;  % No obstacle starting conditions
% X0 = 15; Y0 = 5;  % Small environment starting point with obstacles.
X0 = 60; Y0 = 20;  % Robot initial conditions in environment.

v = 1;             % Robot's velocity.
sr = 4;            % Robot sensor range
nc = 4;            % Number of columns in map division
nr = 4;            % Number of rows in map division

%% Specify the simulated environment and dynamic system initial conditions
% Environment
% xmin = 0; xmax = 50;
% ymin = 0; ymax = 50;
% xmin = 0; xmax = 100;
% ymin = 0; ymax = 100;
xmin = 0; xmax = 200;  % X Dimensions
ymin = 0; ymax = 200;  % Y Dimensions

% Obstacles [x_upper_left, y_upper_left; x_lower_right, y_lower_right]
% Small Obstacles (50-by-50 environment)
% obs1 = [[ 0.0, 12.5]', [ 0.0, 12.5]']; 
% obs2 = [[ 0.0, 12.5]', [37.5, 50.0]'];
% obs3 = [[15.0, 35.0]', [15.0, 35.0]'];
% obs4 = [[37.5, 50.0]', [ 0.0, 12.5]'];
% obs5 = [[37.5, 50.0]', [37.5, 50.0]'];
% Large Obstacles (100-by-100 environment)
% obs1 = [[ 0,  25]', [ 0,  25]'];
% obs2 = [[ 0,  25]', [75, 100]'];
% obs3 = [[30,  70]', [30,  70]'];
% obs4 = [[75, 100]', [ 0,  25]'];
% obs5 = [[75, 100]', [75, 100]'];
% Large Obstacles (200-by-200 environment)
obs1 = [[  0,  50]', [  0,  50]'];
obs2 = [[  0,  50]', [150, 200]'];
obs3 = [[ 60, 140]', [ 60, 140]'];
obs4 = [[150, 200]', [  0,  50]'];
obs5 = [[150, 200]', [150, 200]'];
obs = [obs1,obs2,obs3,obs4,obs5];
% obs = [];  % Use an empty matrix for an obstacle free environment

% Randomly generated environment.
% rng(42);
% obs = generateRandomObstacles(xmax/sr, ymax/sr, 200);
% obs = (obs - 1*mean(obs)) + mean(obs);

% Arnold system
x = 0; y = 1; z = 0;          % Arnold system initial conditions.
A = 0.5; B = 0.25; C = 0.25;  % Arnold System parameters.

% Logistic Map system
x0 = 0.1;  % Initial condition.
r = 4;     % Logistic Map parameter.

% Scaling factor from Arnold system to Robot system.
f = 1.5;  % 1 if no scaling used.

%Initial condition is stored as a column vector.
IC_vec = [x, y, z, X0/f,Y0/f];

% Criterian factor for determining switching between current and new
% trajectory.
c = 0.25;  % Normal Arnold-Logistic system searching
% c = 0.0;  % Arnold system only
% c = inf;  % Arnold-Logistic system searching that always changes zones
          % after a fixed zone search period.

%% Calculate the coverage time
tmax = inf;
for c = c %(0.05:0.05:1) % (0.1:0.05:1)
    for f = f %(1.25:0.25:6)
        for dsi = 3 % [1,2,3]
            fprintf("(c,f,dsi) = (%.2f, %1.2f, %i) -> ", c, f, dsi);
            
            %Initial condition is stored as a column vector.
            IC_vec = [x, y, z, X0/f,Y0/f];

            % Time search
            % try
                [coverage_time] = ...
                    ArnoldLogistic_coverage(A,B,C,...
                                            r,v,...
                                            IC_vec,...
                                            x0,...
                                            c,f,sr,...
                                            xmin,xmax,ymin,ymax,...
                                            obs, nc, nr, dsi, tmax);
                    fprintf(" %f [s] search time.\n", coverage_time);
                tmax = coverage_time;
            % catch  Exception
            %   fprintf("Not optimal\n");
            % end
        end
    end
end
                        