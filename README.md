# README

**Online Search of Unknown Terrains**

1. Authors
    * Karan Sridharan
    * Patrick McNamee
    * Zahra Nili
    * Jeffrey Hudack
2. Date: October 4th, 2021
3. Citation:

```
﻿@Article{Sridharan2022,
author={Sridharan, Karan
and McNamee, Patrick
and Nili Ahmadabadi, Zahra
and Hudack, Jeffrey},
title={Online Search of Unknown Terrains Using a Dynamical System-Based Path Planning Approach},
journal={Journal of Intelligent {\&} Robotic Systems},
year={2022},
month={Sep},
day={06},
volume={106},
number={1},
pages={21},
abstract={Surveillance and exploration of large environments is a tedious task. In spaces with limited environmental cues, random-like search is an effective approach as it allows the robot to perform online coverage of environments using simple algorithm designs. One way to generate random-like scanning search is to use nonlinear dynamical systems to impart chaos into the searching robot's controller. This will result in the generation of unpredictable yet deterministic trajectories, allowing designers to control the system and achieve a high scanning coverage of an area. However, the unpredictability comes at the cost of increased coverage time and a lack of scalability, both of which have been ignored by the state-of-the-art chaotic path planners. This work introduces a new, scalable technique that helps a robot to steer away from the obstacles and cover the entire search space in a short period of time. The technique involves coupling and manipulating two chaotic systems to reduce the coverage time and enable scanning of unknown environments with different online properties. Using this new technique resulted in an average 49{\%} boost in the robot's performance compared to the state-of-the-art planners. The overall search performance of the chaotic planner remained comparable to optimal systems while still ensuring unpredictable paths.},
issn={1573-0409},
doi={10.1007/s10846-022-01707-z},
url={https://doi.org/10.1007/s10846-022-01707-z}
}
```

## Project Description

This project is the code portion of a JIRS paper for Online Search of Unknown Terrains. If you are wishing to simulate the robotic search, the command in the MATLAB IDE that should be done is
    
> Arnold_coverage_rand.m

within the project directory. Rest of the README goes over correspondance between paper and codebase and gives descriptions of the code files.

## Paper Correspondence

### Algorithms

+ Algorithm 1 (*Chaotic Path Planning Algorithm*): Sections 1 and 2 of `ArnoldLogistic_coverage.m`.
+ Algorithm 2 (*MapZoning*): Code sections 3 and 4 of ArnoldLogistic_coverage.m`.
+ Algorithm 3 (*SystemScaler*): Section 2 of `ArnoldLogistic_coverage.m`.
+ Algorithm 4 (*LogisticObstacleAvoid*): Corresponds to `mirrormap_obs_logistic.m`.

### Chaos Manipulation Techniques
    
1. Orientation Control by changing dynamical system variable index on line 276 of `Arnold_coverage_rand.m`.
2. Map Zoning is controlled by `c` value in `Arnold_coverage_rand.m`.
3. System Scaling is controlled by `f` value in `Arnold_coverage_rand.m`.
    
## Files

### Arnold_coverage_rand.m

#### Description

This is the main file creates the environment. It defines the dimensions of the overall rectangular map, the number of rows and columns to discritize the map, and the objects if any that exists. This also defines the initial conditions of both the  Arnold and Logistic systems and the robot parameters.

#### Calls

* `ArnoldLogistic_coverage.m`

#### Called from

None

### ArnoldLogistic_coverage.m

#### Description

This corresponds to Algorithm 1 in the paper. It can be broken down into a couple of sections:

1. Lines 45-116: sets up the algorithm parameters by defining variables to handle what portions of the map has been covered as well as what map cells can be reached. Additionally, parameters that handle path length and search attempts are also defined here along with the initial Arnold system index.
2. Lines 119-236: Generates a path for the robot using the Arnold system to determine coverage for the trajectory
3. Lines 238-266: Calculates what cells in the map the robot has visited and updates the `cell_visit_map` which is keeping track of which cells have been visited.
4. Lines 270-484: Determines which zone, i.e. collection of cells, to visit next based on the coverage density.
5. Lines 490-574: Generates the plots for the corresponding paper.

In addition to simulating the robot's search through the map, plots will appear to show the coverage as well as a generated AVI video file of the search.

#### Calls

* `arnold_RK4.m`
* `cellmap.m`
* `LogisticEquation.m`
* `mirrormap.m`
* `mirrormap_obs.m`
* `mirrormap_obs_arnold.m`
* `mirrormap_obs_logistic.m`
* `prob_zone.m`

#### Called from

* `ArnoldLogistic_coverage.m`


### arnold_RK4.m

#### Description

Updates states of Arnold system using a Runge-Kutta 4th order method. Additionally calculates the next robot trajectory point using the specified index.

#### Calls

None

#### Called from

* `ArnoldLogistic_coverage.m`

### cellmap.m

#### Description

Calculates the number of cells traversed for a given a robot trajectory.

#### Calls

None

#### Called from

* `ArnoldLogistic_coverage.m`

### LogisticEquation.m

#### Description

Uses the Logistic system rather than the Arnold system for generating robot trajectories.

#### Calls

#### Called from

* `ArnoldLogistic_coverage.m`

### mirrormap.m

#### Description

Ensures trajectory points are inside the map.

#### Calls

None

#### Called from

* `ArnoldLogistic_coverage.m`
* `LogisticEquation.m`

### mirrormap_obs_arnold.m
#### Description

Wrapper for `mirrormap_obs` but adjusts the offset for obstacle avoidance.

#### Calls

* `mirrormap_obs.m`

#### Called from

* `ArnoldLogistic_coverage.m`

### mirrormap_obs_logistic.m
#### Description

Ensure obstacle free trajectory to the desired zone as part of the Arnold Logistic Hybrid system. 

#### Calls

* `mirrormap_obs.m`

#### Called from

* `ArnoldLogistic_coverage.m`

### mirrormap_obs.m
#### Description

Mirror mapping for obstacle avoidance.

#### Calls

None

#### Called from

* `ArnoldLogistic_coverage.m`
* `mirrormap_obs_arnold.m`
* `mirrormap_obs_logistic.m`
* `mirrormap_obs.m`

### prob_zone.m
#### Description

Determines the "probability" i.e. density of cell coverage per zone in order to return what zone has the least current coverage at the closest distance.

#### Calls

None

#### Called from

* `ArnoldLogistic_coverage.m`
