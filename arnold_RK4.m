function [x,y,z,X,Y] = arnold_RK4(A,B,C,v,x,y,z,X,Y,dt,index)
% 
% ARNOLD_RK4: Single time step of the Arnold and robot systems using an RK4
% 
% Description: Given the parameters and current locations of both the Arnold
%              and robot systems, it calculates the next location using a
%              Runge-Kutta 4th order solver. 
% 
% Input:
%     x, y, z = Arnold system coordinates
%     X, Y    = Robot system coordinates
%     A, B, C = Arnold system parameters
%     v       = Robot velocity
%     dt      = time step
%     index   = An index that decides which Arnold system coordinate to use.
%               Values are from 1-3 corresponding to x, y, and z 
%               respectively.
% 
% Output:
%     x, y, z = Updated Arnold system coordinates
%     X, Y    = Updated Robot coordinates
%             
% Author: Karan Sridharan
%         Patrick McNamee
% Date: Unknown
% 
% Edits:
%     09/13/2021: Reformatted code and comments -- Patrick McNamee
% 

    % Differential measurements
    k1 = arnold(A, B, C, x, y, z);
    k2 = arnold(A, B, C,...
        x + 0.5*dt*k1(1), y + 0.5*dt*k1(2), z + 0.5*dt*k1(3));
    k3 = arnold(A, B, C,...
        x + 0.5*dt*k2(1), y + 0.5*dt*k2(2), z + 0.5*dt*k2(3));
    k4 = arnold(A, B, C, x + dt*k3(1), y + dt*k3(2), z + dt*k3(3));
   
    % Weighted sum of step
    x = x + (dt/6)*(k1(1) + 2*k2(1) + 2*k3(1) + k4(1));
    y = y + (dt/6)*(k1(2) + 2*k2(2) + 2*k3(2) + k4(2));
    z = z + (dt/6)*(k1(3) + 2*k2(3) + 2*k3(3) + k4(3));
    
    %Array storing the angles to use for the trajectory
    dynam_angle = [x,y,z];

    % Updating the robot system
    X = X + dt*v*cos(dynam_angle(index));
    Y = Y + dt*v*sin(dynam_angle(index));
end

function [dxvec] = arnold(A, B, C, x, y, z)
    % Arnold system definition
    dx = A*sin(z) + C*cos(y);
    dy = B*sin(x) + A*cos(z);
    dz = C*sin(y) + B*cos(x);
    dxvec = [dx, dy, dz];
end

