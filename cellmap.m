function [cell_visit_map, new_cells_covered, rep_cells_covered] = ...
    cellmap(xpoints,ypoints,px,py,cell_visit_map,reachable_cells)
% 
% CELLMAP find the number of cells the robot visited along the path.
% 
% Usage: [cell_visit_map new_cells_covered rep_cells_covered] = ...
%     cellmap(xpoints,ypoints,px,py,cell_visit_map,reachable_cells)
% 
% Description: Calculates the number of cells the robot visited along the
%     path p given the discritization of the 2D environment into cells.
% 
% Input:
%     xpoints: grid points located in the x-axis
%     ypoints: grid points located in the y-axis
%     px: vector, p containing the X coordinates of the Robot's trajectory
%     py:  vector, p containing the Y coordinates of the Robot's trajectory
%     cell_visit_map: Map that denotes the number of cells the robot visited
% 
% Output:
%     cell_visit_map    = Array that denotes the number of cells the robot
%                         visited new_cells_covered: Tracks the number of 
%                         new cells being covered at the entire map.
%     rep_cells_covered = Tracks the number of cells repeated at the entire 
%                         map.
% 
% Author: Karan Sridharan
%         Patrick McNamee
% Date: Unknown
% 
% Edit:
%     09/14/2021 -- Refactored code and comments (Patrick McNamee)
%
    
%Assigns it to zero for calculation
new_cells_covered = 0; rep_cells_covered = 0;
%loops across the entire column in each row to determine how many cells the robot visited
for i = 1:length(ypoints)-1
    for j = 1:length(xpoints)-1
        
        % Skip unreachble cells
        if ~reachable_cells(i,j)
            continue;
        end
        
         number_cell_visited = 1; % Number of times the cell is visited.
         
        %k is an index value that gets the index number of points that are lying
        %within the boundares of a particular cell. The purpose of this
        %step is to identify whether each point occurs at the cell at
        %instantaneous time or at a later time
        k = find(px >= xpoints(j)  &...
                 px <= xpoints(j+1)&...
                 py >= ypoints(i)  &...
                 py <= ypoints(i+1));

        %The following code checks whether a cell has been visited. It
        %then checks whether the Robot is still in the current cell, it
        %has been in last time and also checks if the Robot is
        %visiting the same cell as iteration changes in current
        %time
        if ~isempty(k) %if there are points in a cell, moves to line 39 onwards
            if cell_visit_map(i,j) ~= 0 %if the algorithm registeres a robot has visited the cell last time
                %If k(1) is 1 then the Robot is still in the current cell in the current and last time
                if k(1) == 1 
                    if length(k) == 1 %if the algorithm registers one point in the cell
                        continue %goes to the next column, j + 1
                    elseif length(k) > 1 %if the algorithm registers multiple points in the cell
                        for l = 1:length(k)-1
                            %if the difference between the index points
                            %is 1, it means the robot is still in the
                            %cell, at time t. If the difference is
                            %greater than 1 then at some point in time,
                            %t the robot has visited the same cell
                            %again
                            if k(l+1)-k(l) == 1
                                continue;
                            else
                               number_cell_visited = number_cell_visited + 1; %some point in time, the robot has visited that cell
                            end
                            cell_visit_map(i,j) = cell_visit_map(i,j) + number_cell_visited; %assigns the number of times the robot visited to that cell
                            rep_cells_covered = rep_cells_covered + 1; %increments the repeated cells by 1 showing the robot has visited that cell again
                        end
                    end
                else %some point in time, the robot has returned to that cell
                    if length(k) > 1 
                    %As iteration increases, the Robot would have
                    %visited this cell at a later instant of time
                        for l = 1:length(k)-1
                            if k(l+1)-k(l) ~= 1
                                number_cell_visited = number_cell_visited + 1; %some point in time, the robot has visited that cell
                            end
                        end
                   cell_visit_map(i,j) = cell_visit_map(i,j) + number_cell_visited; %assigns the number of times the robot visited to that cell
                   rep_cells_covered = rep_cells_covered + 1; %increments the repeated cells by 1 showing the robot has visited that cell again

                    elseif length(k) == 1 %if there is one point in the cell, it still means the robot has visited that cell later in time t
                     cell_visit_map(i,j) = cell_visit_map(i,j) + number_cell_visited; %assigns the number of times the robot visited to that cell
                     rep_cells_covered = rep_cells_covered + 1; %increments the repeated cells by 1 showing the robot has visited that cell again
                    end
                end
            %This line will execute if a cell has not been covered in the last iteration    
            else
                 if length(k) ~= 1
                     % TODO: replace for loop with simple array difference
                     for l = 1:length(k)-1 
                    %What this line means is say for instance at index
                    %numbers 86 and 96, the points are lying between x =
                    %-0.25 and -0.25 and y = -2.5 and -2.45. At the 96th
                    %index, the Robot has returned to the same cell at a
                    %later time. Therefore, this condition says that if the
                    %Robot has returned to the same cell at a later time,
                    %add that to the number of times the Robot has visited
                    %that cell
                    if k(l+1)-k(l) ~= 1
                        number_cell_visited = number_cell_visited + 1; %some point in time, the robot has visited that cell
                    end
                     end
                     %number_cells_visited = sum(k(2:end) - (k(1:end-1)) > 1);
                  cell_visit_map(i,j) = number_cell_visited; %assigns the number of times the robot visited to that cell
                  new_cells_covered = new_cells_covered + 1; %since the robot visited the cell first time, it has covered a new cell

                  if cell_visit_map(i,j) > 1 %in case the robot visits a cell first time and comes back to the same cell, at a later time
                      rep_cells_covered = rep_cells_covered + 1; %increments the repeated cells by 1 showing the robot has visited that cell again
                  end

                 end
                 %Suppose there are only one point that lie in a particular cell, the the
                 %cell map will register that cell as been covered once
                 if length(k) == 1
                     cell_visit_map(i,j) = number_cell_visited; %some point in time, the robot has visited that cell
                     new_cells_covered = new_cells_covered + 1; %since the robot visited the cell first time, it has covered a new cell
                 end
            end
        end
    end
end
