function obs_array = generateRandomObstacles(Nx, Ny, Nfilled)
% GENERATERANDOMOBSTACLES
%
%     Usage: obs_array = generateRandomObstacles(Nx, Ny, Nfilled)
%
%     Description: Given a number of rows and columns, randomly fills in
%                  squares. Rules are that squares can form horizontal or
%                  vertical rectangles but all rectangles must be more than
%                  1 cell buffer from any other rectangle.
%
%     Input:
%         Nx      = integer number of x discretization
%         Ny      = integer number of y discretization
%         Nfilled = maximum number of filled cells
%         
%     Output:
%         obs_array = 2x(2M) array of M obstacles.
%         
%     Author: Patrick McNamee
%     Date: January 24th, 2022
%     Edits:
    
    % Initialize the grid and occupancy count
    grid = zeros(Nx, Ny);
    % grid[i,j] = 1 means an obstacle occupies the grid.
    % grid[i,j] = 0 means a free space where an obstacle can be placed.
    % grid[i,j] = -1 means a free space where an obstacle cannot be placed.
    n_filled = 0;  % At the start, no grid cells have been occupied.
    
    % Randomly fill in the grid with obstacles while ensuring no disjoint
    % free spaces are created.
    while n_filled < Nfilled
        valid_ind = find(grid == 0);  % find where obstacles can be placed.
        if isempty(valid_ind)
            % Stop if there are no valid spaces to place more obstacles
            break
        else
            % Get a random grid position to place obstacles
            [i, j] = ind2sub([Nx, Ny], valid_ind(randi(size(valid_ind,1))));
            
            % Mark grid cell as occupied and update fill count
            grid(i,j) = 1;
            n_filled = n_filled + 1;
            
            % Update the grid to reflect the now invalid cells
            grid = invalidateGridCells(grid,i,j,Nx,Ny);
        end
    end
    
    % Use function to iterate through grid an get obstacles into array.
    obs_array = obstaclesFromFilledGrid(grid, Nx, Ny);
end

% Some helper functions
function grid = invalidateGridCells(grid, i, j, Nx, Ny)
    % Invalidate Diagonals
    for u = [-1, 1]
        for v = [-1, 1]
            % If the diagonal cell is in the grid
            if (i + u > 0) && (i + u <= Nx) && ...
               (j + v > 0) && (j + v <= Ny)
                % Mark the cell as invalid
                grid(i+u,j+v) = -1;
            end
        end
    end

    % Invalidate vertical spaces if left adjacent is occupied
    if i > 1
        if grid(i-1,j) > 0
            if j < Ny
                grid(i,j+1) = -1;
            end
            if j > 1
                grid(i,j-1) = -1;
            end
        end
    end
    % Invalidate vertical spaces if right adjacent is occupied
    if i < Nx
        if grid(i+1,j) > 0
            if j < Ny
                grid(i,j+1) = -1;
            end
            if j > 1
                grid(i,j-1) = -1;
            end
        end
    end
    % Invalidate horizontal spaces if lower adjacent is occupied
    if j > 1
        if grid(i,j-1) > 0
            if i < Nx
                grid(i+1,j) = -1;
            end
            if i > 1
                grid(i-1,j) = -1;
            end
        end
    end
    % Invalidate horizontal spaces if upper adjacent is occupied
    if j < Ny
        if grid(i,j+1) > 0
            if i < Nx
                grid(i+1,j) = -1;
            end
            if i > 1
                grid(i-1,j) = -1;
            end
        end
    end
end

function obs_array = obstaclesFromFilledGrid(grid, Nx, Ny)
    obs_array = [];
    i = 1;
    j = 1;
    
    % Iterate through grid from left to right, bottom to top.
    while j <= Ny
        % If current grid cell is occupied
        if grid(i,j) == 1
            % Mark cell boundaries
            l = i-1;
            r = i;
            b = j-1;
            t = j;
            % Go right to edge of obstacle chain
            while i < Nx
                if grid(i+1,j) == 1
                    % Triggers if ?-(i,j)-o-?
                    r = r+1;
                    i = i+1;
                else
                    % Triggers if ?-(i,j)-i or ?-(i,j)|
                    break
                end
            end
            % Go up to edge of obstacle chain
            k = j;
            while k < Ny
                if grid(i,k+1) == 1
                    t = t+1;
                    k = k+1;
                else
                    break
                end
            end
            
            % Place obstacle into array
            obs_array = horzcat(obs_array, [l,b;r,t]);
            % Remove obstacle from grid
            grid(l+1:r,b+1:t) = -1;
        end
        % Increment search subscripts
        i = i+1;
        if i > Nx
            j = j+1;
            i = 1;
        end
    end
end