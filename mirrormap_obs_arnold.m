function [Xcoord, Ycoord] = ...
    mirrormap_obs_arnold(Xcoord,Ycoord,obs_vec,xmin, xmax, ymin, ymax, fac)
% MIRROR_OBS_ARNOLD wrapper for mirrormap_obs
% 
% Usage: [Xcoord, Ycoord] = mirrormap_obs_arnold(Xcoord,Ycoord,obs_vec,xmin, xmax, ymin, ymax)
%
% Input:
%     Xcoord                 = robot X position
%     Ycoord                 = robot Y position
%     obs_vec                = vector consisting of obstacle coordinates
%     xmin, xmax, ymin, ymax = map boundaries
%     fac                    = tolarence for obstacle
% 
% Output:
%     Xcoord = robot X position after potential mapping
%     Ycoord = robot Y position after potential mapping
% 
% Author: Karan Sridharan
%         Patrick McNamee
% Date: 09/14/2021
% 
% Edit:
%     09/14/2021 -- Reformatted code and comments (Patrick McNamee)
%

if isempty(fac)
    fac = 1.;
end

[Xcoord, Ycoord] = ...
    mirrormap_obs(Xcoord,Ycoord,obs_vec,xmin, xmax, ymin, ymax, fac);

end
