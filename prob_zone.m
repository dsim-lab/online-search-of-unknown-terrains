function [zone, probzone_vec] = ...
    prob_zone(cell_visit_map,reachable_cells,...
              xmin,xmax,ymin,ymax,X_begin,Y_begin,...
              n_zr,n_zc,SR)
% 
% Obtains the midpoint coordinates associated with the best zone
% 
% Inputs:
%     cell_visit_map- array containing the number of cells visited
%     xmin, xmax- horizontal boundaries of the map
%     ymin, ymax - vertical boundaries of the map
%     X_begin,Y_begin - robot IC of the Henon map system
% 
% Outputs
% X_end,Y_end - desired midpoint coordinates
% 
% Author: Karan Sridharan
%         Patrick McNamee
% Date: Unknown
% 
% Edit:
%     09/22/2021 -- Reformated and updated code/comments (Patrick McNamee)
%

R = (xmax - xmin)/n_zr;  % size of zone in each row
C = (ymax - ymin)/n_zc;  % size of zone in each column
n_cell_r = R/SR;  % number of cells in each zone in the row
n_cell_c = C/SR;  % number of cells in each zone in the column

% cell_visit_map(cell_visit_map > 1) = 1;
% Mask visit array to prevent counting unreachable zones
cell_visit_map = cell_visit_map.*reachable_cells;

% Preallocate zone information
probzone_vec = zeros(n_zr*n_zc, 5);
probzone_vec(:,1) = (1:n_zr*n_zc)';

k=0;
for i=1:n_zr
    for j=1:n_zc
        k=k+1;
        % Determine index of cells of interest
        if i == 1
            row_index = 1:floor(n_cell_c);
        else
            row_index = floor((i-1)*n_cell_c)+1:floor(i*n_cell_c);
        end
        if j == 1
            col_index = 1:floor(n_cell_r);
        else
            col_index = floor((j-1)*n_cell_r)+1:floor(j*n_cell_r);
        end

        % Determine the number of viable cells in zone
        nc_z = sum(reachable_cells(row_index, col_index), 1:2);
        % nc_z = 1;  % Debugging purposes
        % Calculate the uniform weighted coveraged probability
        probzone_vec(k,2) = ...
            sum(cell_visit_map(col_index, row_index), 1:2)/nc_z;
    end
end

%identifies which zone the robot is currently at
k=0;
zone = [];
for j=1:n_zr
    for i=1:n_zc
        k=k+1;
        % Calculate the midpoints
        probzone_vec(k,3)=(R/2)+(j-1)*R;
        probzone_vec(k,4)=(C/2)+(i-1)*C;
        % distance from the robot's current location and the zone midpoint
        probzone_vec(k,5) = sqrt(...
            (probzone_vec(k,3)-X_begin)^2 + ...
            (probzone_vec(k,4)-Y_begin)^2 ...
        );
        if X_begin>=(probzone_vec(k,3)-C/2) && X_begin<=(probzone_vec(k,3)+C/2) && ...
           Y_begin>=(probzone_vec(k,4)-R/2) && Y_begin<=(probzone_vec(k,4)+R/2) && ...
           isempty(zone)
            zone=k;
        end
        
    end
end

%finds the minimum density zone
k = find(probzone_vec(:,2) == min(probzone_vec(:,2)));

%the following line of code checks whether the minimum density zone is at
%the same location as the robot's current position. In this case, it will
%regenerate a new set of indices corresponding the the probzone_vec,
%excluding the index corresponding to the robot's current location

if length(k) == 1 && k(1) == zone
    m = find(probzone_vec(:,2) ~= probzone_vec(zone,2));
    k = find(probzone_vec(:,2) == min(probzone_vec(m,2)));
end

%if multiple zones correspond to the minimum density, the following code
%determines the minimum distance between the robot's current location and the
%new zone is

%generates the distance between the robot's current location and all
%possible zones
if length(k) ~= 1
    p = find(probzone_vec(k,5) == min(probzone_vec(k,5))); %finds the minimum distance from the list
else
    p = 1;
end
%assigns the index associated with minimum density and/or distance to
%the zones midpoint coordinates

end

